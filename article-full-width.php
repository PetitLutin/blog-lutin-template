<?php include(dirname(__FILE__) . '/header.php'); ?>

<main class="main">

	<div class="container">

		<div class="grid">

			<div class="content col sml-12">


				<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

					<header>
						<span class="art-date">
							<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
								<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
							</time>
						</span>
						<h2>
							<?php $plxShow->artTitle(); ?>
						</h2>
						<div>
							<small>
								<span class="written-by">
									<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
								</span>
								<span class="art-nb-com">
									<a href="<?php $plxShow->artUrl(); ?>#comments"
										title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
								</span>
							</small>
						</div>
						<div>
							<small>
								<span class="classified-in">
									<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
								</span>
								<span class="tags">
									<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
								</span>
							</small>
						</div>
					</header>

					<?php //$plxShow->artThumbnail(); ?>
					<?php //$plxShow->artContent(); ?>
					<!-- on n'affiche pas le chapo -->
					<div class="chapo">
						<?php //$plxShow->artChapo('', false); ?>
					</div>
					<!-- on affiche le contenu de l'article sans le chapo -->
					<?php $plxShow->artContent(false); ?>
				</article>
				<style>
					.grid {
						padding-left: 15rem;
						padding-right: 15rem;
					}

					fieldset .grid.col.sml-12 label{
						width:100%;
						position: unset;
					}
				</style>
				<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>
				<br >

				<?php echo $plxShow->callHook('prevNext',array(
				false,
				'<p style="float:left;margin-top:0"><a href="#prevUrl" rel="prev">&laquo; #prevTitle</a></p>'."\n\t\t\t\t\t",
				'<p style="text-align:right;"><a href="#nextUrl" rel="next">#nextTitle &raquo;</a></p>'
			)); ?>
				<?php include(dirname(__FILE__).'/commentaires.php'); ?>

			</div>

		</div>

	</div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>