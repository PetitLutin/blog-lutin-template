<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

	<div class="container">

		<div class="grid">

			<div class="content col sml-12">

				<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

					<header>

						<h1>
							<?php $plxShow->artTitle(); ?>
							<?php eval($plxShow->callHook('share_me')); ?>
						</h1>
						<span class="art-date">
							<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
								<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
							</time>
						</span>

					</header>

					<?php //$plxShow->artThumbnail(); ?>
					<?php //$plxShow->artContent(); ?>

					<!-- on n'affiche pas le chapo -->
					<div class="chapo">
						<?php //$plxShow->artChapo('', false); ?>
					</div>
					<?php eval($plxShow->callHook('MyMultiLingue', 'artlinks')) ?>
					<!-- on affiche le contenu de l'article sans le chapo -->
					<?php $plxShow->artContent(false); ?>
					
					
					<!-- INCENTIVE -->
					<div class="text-center" style="margin-top:45px;">
						<?php include(dirname(__FILE__).'/incentive.php'); ?>
					</div>

					<div>
						<small>
							<span class="written-by">
								<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
							</span>
							<span class="art-nb-com">
								<a href="<?php $plxShow->artUrl(); ?>#comments"
									title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
							</span>
						</small>
					</div>
					<div>
						<small>
						 <span class="classified-in">
								<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
							</span>
							<span class="article tags">
								<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
							</span> 
						</small>
					</div>
					
					<p>
							<div class="patronage">
							<?php if ($_SESSION['lang']=='fr' ){  ?>
								Soutenez le blog :
							
							<?php  	} else if ($_SESSION['lang']=='en') { ?>
								Support the blog:
							<?php } ?>
								<span><a href="https://ko-fi.com/petitlutinartiste"><img src="<?php $plxShow->racine() ?>/data/medias/kofi_blanc.png" width="25" height="auto"></a></span>
								<span style="margin-top:-2px"><a href="https://fr.tipeee.com/petit-lutin-artiste"><img src="<?php $plxShow->racine() ?>/data/medias/tipeee_tip_btn.svg" width="25"></a></span>
								<span><a href="https://liberapay.com/petitlutinartiste/"><i class="fa fa-liberapay" aria-hidden="true"></i></a></li>

							</div>
						</p>

					<div class="newslutin-rss">
						<?php if ($_SESSION['lang']=='fr' ){  ?>
						Pour être informé·e de chaque nouvel article sans être dépendant·e d'un réseau "social", 
						abonnez-vous à la <a href="<?php $plxShow->racine() ?>newsletter" title="Newsletter">newsletter <img src="<?php $plxShow->racine() ?>data/medias/newslutin_icon3.svg" width="25"></a>
						 ou à mon <a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="RSS">flux RSS <i class="fa fa-rss"></i></a>.
						<?php  
						}
						else if ($_SESSION['lang']=='en')
							{ ?>
							Be informed of the latest blog post without depending on a "social" media, subscribe to the <a href="<?php $plxShow->racine() ?>en/newsletter" title="Newsletter">newsletter <img src="<?php $plxShow->racine() ?>data/medias/newslutin_icon3.svg" width="25"></a> 
							or the <a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="RSS">RSS feed <i class="fa fa-rss"></i></a>.
							
						<?php }
						?> 
		
					</div>

				</article>


				<style>
					.container > .grid{
						padding-left:2rem;
						padding-right:2rem;
					}

					@media all and (min-width:1000px){
						.container > .grid{
							padding-left:15rem;
							padding-right:15rem;
						}
					}

					.patronage{
						display:flex;
						align-items:center;
						gap:10px;
					}
					.article-special h2{
						font-size:2em;
					}
					.article-special-title{
						text-align:center;
					}
					.img-boutique{
						background:url('<?php $plxShow->racine() ?>data/medias/christmas-shop/aperçu-desktop.png') no-repeat top left;
						height:700px;
						border:2px dashed #5898c4;
						box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
					}
					.img-shop{
						background:url('<?php $plxShow->racine() ?>data/medias/christmas-shop/preview-desktop.png') no-repeat top left;
						height:700px;
						border:2px dashed #5898c4;
						box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
					}

					.img-boutique:hover, .img-shop:hover{
						border:2px dashed #C4559F;
					}

					.img-boutique .alt, .img-shop .alt{
						visibility:collapse;
					}

					/* .grid-products{
						display: grid;
						grid-template-columns: repeat(2,1fr);
						gap: 10px;
						margin:25px 0 15px;
					}
					.grid-product{
						display: flex;
						flex-direction: column;
						align-items: center;
					}
					.grid-product img{
						width:100%;
						max-width:350px;
						max-height:350px;
						object-fit:cover;
					}
					
					.grid-product div{
						text-align: center;
					} */
					@media all and (max-width:500px){
						form fieldset .grid.col.sml-12 > label{
							width:100%!important;
							position: relative!important;
							right: unset!important;
						}	
						.patronage{
							flex-wrap:wrap;
						}
					}

					.komika {
						font-size:1.1em;
					}

					@media all and (min-width:1100px){
						.article-special{
							margin-top:40px;
						}
					}

					.article header h1{
						font-weight: unset;
					}
					
				</style>


				<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>
				<br>
				<!-- 
				<?php //echo $plxShow->callHook('prevNext',array(
				// false,
				// '<div style="display:flex; flex-wrap:wrap; justify-content:space-between;">
				// <div><a href="#nextUrl" rel="next">&laquo; #nextTitle</a></div>
				// <div><a href="#prevUrl" rel="prev">#prevTitle &raquo;</a></div>
				// </div>',''
			// )); ?>  -->

				<?php echo $plxShow->callHook('prevNext',array(
				false,
				'<p style="float:right;"><a href="#prevUrl" rel="prev">#prevTitle &raquo</a></p>'."\n\t\t\t\t\t",
				'<p style="text-align:left;margin-top:0"><a href="#nextUrl" rel="next">&laquo; #nextTitle</a></p>'
			)); ?>


				<?php include(dirname(__FILE__).'/commentaires.php'); ?>

			</div>

			<?php //include(dirname(__FILE__).'/sidebar.php'); ?>

		</div>

	</div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
