<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main">

		<div class="container">

			<div class="grid">

				<div class="content col sml-12 med-9">

					<ul class="repertory menu breadcrumb">
						<li><a href="<?php $plxShow->racine() ?>"><?php $plxShow->lang('HOME'); ?></a></li>
						<li><?php $plxShow->catName(); ?></li>
					</ul>

					<p><?php $plxShow->catDescription('#cat_description'); ?></p>

					<!-- <?php //while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

					<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

						<header>
							<span class="art-date">
								<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
									<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
								</time>
							</span>
							<h2>
								<?php $plxShow->artTitle('link'); ?>
							</h2>
							<div>
								<small>
									<span class="written-by">
										<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
									</span>
									<span class="art-nb-com">
										<?php $plxShow->artNbCom(); ?>
									</span>
								</small>
							</div>
							<div>
								<small>
									<span class="classified-in">
										<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
									</span>
									<span class="tags">
										<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
									</span>
								</small>
							</div>
						</header>

						<?php //$plxShow->artThumbnail(); ?>
						<?php //$plxShow->artChapo(); ?>

					</article>
					
					<?php //endwhile; ?> -->


					<div class="oldArticles">
					<?php $plxShow->lastArtList('<li> <a href="#art_url" title="#art_title">#art_chapo</a> </li>',15) ?>

				</div>
				<style>
					.container {
						/* max-width: 1280px; */
						/* width: 100%; */

					}

					.grid {
						/*padding: 0;
						margin: auto;*/
						display: flex;
						flex-wrap: wrap;
					}

					.oldArticles {
						display: flex;
						flex-wrap: wrap;
						width: 100%;
						padding: 0;
						justify-content: center;
						margin: auto;
						width: 100%;
					}

					.oldArticles li {
						list-style-type: none;
						margin: 15px;
						min-width: 270px;
						height: 300px;
						box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
						text-align: center;
					}

					.oldArticles li p {
						margin: 0;
						padding: 0;
						width: 270px;
					}

					.oldArticles li img {
						width: 270px;
						height: 200px;
						object-fit: cover;
						padding: 0;
						margin: 0;
					}

					@media all and (min-device-width:1100px) {
						.container {
							padding-left: 3rem !important;
							padding-right: 3rem !important;
						}
					}

					@media all and (max-device-width: 400px) {
						/*  */

					}
				</style>

					<nav class="pagination text-center">
						<?php $plxShow->pagination(); ?>
					</nav>

					<span>
						<?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
					</span>

				</div>

				<?php //include(dirname(__FILE__).'/sidebar.php'); ?>

			</div>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>