<?php include(dirname(__FILE__).'/header.php'); ?>

<section>

    <div class="content">
<!-- changement de class en full-width -->
        <div class="full-width">

                        <?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

<!-- appel de la vignette et du titre de l'article avec ajout de nouvelles class -->
<!-- + suppression de pas mal de choses (auteur, date, tags, etc) -->
                
            <article role="article" id="post-<?php echo $plxShow->artId(); ?>" class="vignette-container">
                <section>
                    <a href="<?php $plxShow->artUrl() ?>">
                        <div class="vignette-img">

                            <img src="<?php $plxShow->template(); ?><?php eval($plxShow->callHook("showVignette")); ?>	" alt="<?php $plxShow->artTitle(); ?>" />

                        </div>

                        <div class="vignette-title">
                            <?php $plxShow->artTitle(); ?>
                        </div>
                    </a>
                </section>
            </article>


            <?php endwhile; ?>


<?php while ($plxShow->plxMotor->plxPlugins->callHook('plxShowLastArtList')) ;?>

<?php eval($plxShow->callHook("vignetteArtList", '<li><a href="#art_url" title="#art_title"><img src="#art_vignette" /></a></li>')); ?>	

<div style="clear:both;"></div>
            <div id="pagination">
                <?php $plxShow->pagination(); ?>
            </div>


        </div>

        </div>

</section>

<?php include(dirname(__FILE__).'/footer.php'); ?>