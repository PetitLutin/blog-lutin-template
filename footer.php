<?php if (!defined('PLX_ROOT')) exit; ?>

<footer class="footer">
	<div class="container">
		<p>
			<?php $plxShow->mainTitle('link'); ?> - <?php $plxShow->subTitle(); ?> &copy; 2019-<?php $year = date("Y");echo $year; ?>
		</p>
		<p>
			<!-- <?php //$plxShow->lang('POWERED_BY') ?>&nbsp;<a href="http://www.pluxml.org" title="<?php //$plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a> -->
			<!-- <?php //$plxShow->lang('IN') ?>&nbsp;<?php //$plxShow->chrono(); ?>&nbsp; -->
			<!-- <?php //$plxShow->httpEncoding() ?>&nbsp;-  -->
			<!-- <a rel="nofollow" href="<?php //$plxShow->urlRewrite('core/admin/'); ?>" title="<?php //$plxShow->lang('ADMINISTRATION') ?>"><?php //$plxShow->lang('ADMINISTRATION') ?></a> -->
		</p>
	
	</div>

	<ul class="pub">
		<li><a href="https://framapiaf.org/@Petit_Lutin" title="Mastodon"><i class="fa fa-mastodon"></i></a></li>
		<li> <a href="https://www.facebook.com/PetitLutinArtiste/" title="Fesses-bouc"><i
					class="fa fa-facebook-f"></i></a></li>
		<li><a href="https://www.instagram.com/petit_lutin_artiste/" title="Insta(kilo)gram"><i
					class="fa fa-instagram"></i></a></li>
		<li><a href="https://www.youtube.com/@PetitLutinArtiste" title="Youtube"><i class="fa fa-youtube-play"></i></a></li>
												
		<li><a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="RSS"><i class="fa fa-rss"></i></a></li>
		<li>

			<?php  if ($_SESSION['lang']=='fr' ){  ?>
				<a href="<?php $plxShow->racine() ?>newsletter" title="Newsletter">
			<?php  }
			else if ($_SESSION['lang']=='en') { ?>
				<a href="<?php $plxShow->racine() ?>en/newsletter" title="Newsletter">
			<?php } ?> 
				<img src="<?php $plxShow->racine() ?>data/medias/newslutin_icon3.svg" width="22">
			</a>
		</li>
		<li><a href="https://liberapay.com/petitlutinartiste/"><i class="fa fa-liberapay" aria-hidden="true"></i></a></li>			
		<li class="kofi"><a href="https://ko-fi.com/petitlutinartiste"><img  src="<?php $plxShow->racine() ?>/data/medias/kofi_blanc.png" width="30" ></a></li>
		<li class="tipeee"><a href="https://fr.tipeee.com/petit-lutin-artiste"><img  src="<?php $plxShow->racine() ?>/data/medias/tipeee_tip_btn.svg" width="25" ></a></li>
	</ul>

	<ul>

	<?php if ($_SESSION['lang']=='fr' ){  ?>
		<li><a href="mentions-legales">Mentions Légales</a>&nbsp;- <a href="politique-confidentialite">Politique de confidentialité</a>&nbsp;
						
	<?php  	} else if ($_SESSION['lang']=='en') { ?>	
		<li><a href="<?php $plxShow->racine() ?>en/legal">Legal</a>&nbsp;- <a href="<?php $plxShow->racine() ?>en/privacy-policy">Privacy policy</a>&nbsp;

	<?php } ?> 

		<!-- <li><a href="<?php //$plxShow->urlRewrite('feed.php?rss') ?>" title="<?php //$plxShow->lang('ARTICLES_RSS_FEEDS'); ?>"><?php //$plxShow->lang('ARTICLES'); ?></a></li> -->
		<!-- <li><a href="<?php //$plxShow->urlRewrite('feed.php?rss/commentaires'); ?>" title="<?php //$plxShow->lang('COMMENTS_RSS_FEEDS') ?>"><?php //$plxShow->lang('COMMENTS'); ?></a></li> -->
		
		<li><a href="<?php $plxShow->urlRewrite('#top') ?>"
				title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a></li>
	</ul>

	<?php //eval($plxShow->callHook('plxShowCounters')) ?>
	<?php //eval($plxShow->callHook('MyMultiLingue')) ?>

	<style>
		ul {
			padding: 0;
		}

		li {
			list-style-type: none;
		}

		.pub,
		.categories,
		.tags:not(.article) {
			display: flex;
			padding: 0;
			/* margin: auto; */
			/* justify-content: center; */
		}

		.pub {
			justify-content: center;
		}

		.categoriesEtTages {
			justify-content: space-between;
		}

		.categories, 
		.tags {
			flex-wrap: wrap;
			flex: 50%;
		}

.tags li a {
	font-size: 0.9em;
}
		.pub li {
			padding: 10px;
		}

		.pub li a {
			color: #a47bcd;
			font-size: 1.5em;
		}
	</style>
	</div>
</footer>

</body>

</html>