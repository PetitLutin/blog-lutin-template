<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">

<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>

	<meta property="og:type" content="website" />

		<?php
		// posts and pages have their own og image
		$url = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($url == $plxMotor->aConf['racine']){ ?>

	<meta property="og:url" content="<?php $plxShow->racine() ?>" />
	<meta property="og:image" content="<?php $plxShow->racine() ?>themes/blog/img/petitlutinartiste_abbeyroad-walking-banner.png" />		

		<?php
		}
		?>


	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.2.0/css/fork-awesome.min.css" integrity="sha256-XoaMnoYC5TH6/+ihMEnospgm0J1PM/nioxbOUdnM8HY=" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/plucss.css" media="screen" />
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/theme.css" media="screen" />
	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>"
		href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>"
		href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />


	<!-- Matomo -->
	<script type="text/javascript">
		var _paq = window._paq || [];

		_paq.push([function () {
			var self = this;

			function getOriginalVisitorCookieTimeout() {
				var now = new Date(),
					nowTs = Math.round(now.getTime() / 1000),
					visitorInfo = self.getVisitorInfo();
				var createTs = parseInt(visitorInfo[2]);
				var cookieTimeout = 33696000; // 13 mois en secondes   
				var originalTimeout = createTs + cookieTimeout - nowTs;
				return originalTimeout;
			}
			this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout());
		}]);


		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function () {
			var u = "//stats.petitlutinartiste.fr/";
			_paq.push(['setTrackerUrl', u + 'matomo.php']);
			_paq.push(['setSiteId', '1']);
			var d = document,
				g = d.createElement('script'),
				s = d.getElementsByTagName('script')[0];
			g.type = 'text/javascript';
			g.async = true;
			g.defer = true;
			g.src = u + 'matomo.js';
			s.parentNode.insertBefore(g, s);
		})();
	</script>
	<!-- End Matomo Code -->
</head>

<body id="top" class="page mode-<?php $plxShow->mode(true) ?>">

	<header class="header sticky">

		<div class="container">

			<div class="grid">

				<div class="col sml-hide lrg-show lrg-6">

					<div style="display:flex;justify-content:space-between">
						<a href="<?php $plxShow->racine() ?>">
							<div class="logo">
								<!-- <h1 class="no-margin heading-small"><?php //$plxShow->mainTitle(''); ?></h1> -->
								<span class="h2 no-margin "><?php $plxShow->mainTitle(''); ?></span><br>
								<!-- <h2 class="h5 no-margin"><?php //$plxShow->subTitle(); ?></h2> -->
								<span class="h3 no-margin"><?php $plxShow->subTitle(); ?></span>
							</div>
						</a>
						<span class=""><?php eval($plxShow->callHook('MyMultiLingue')) ?></span>
					</div>
				</div>

				<!-- <div class="col sml-2"><?php //eval($plxShow->callHook('MyMultiLingue')) ?>
				</div> -->
				<div class="col sml-show sml-12 med-12 lrg-6 navbar">
						
					

					<nav class="nav">
						<div class="col sml-show sml-8 med-show med-7 lrg-hide">
									<div style="display:flex;">
										<a href="<?php $plxShow->racine() ?>">
											<div class="logo">
												<div style="display:flex;flex-direction:column">
													<!-- <div class="h4 no-margin" style="white-space:nowrap"><?php //$plxShow->mainTitle(''); ?></div> -->
													<!-- <div class="h6 no-margin" style="white-space:nowrap"><?php //$plxShow->subTitle(); ?></div> -->
												</div>
											</div>
										</a>
										<?php eval($plxShow->callHook('MyMultiLingue')) ?>
									</div>
							</div>

						<div class="bigZIndex">
							<div class="responsive-menu">
								<label for="menu"><span class="prout"></span></label>
								<input type="checkbox" id="menu">
								<ul class="menu">

									<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_class #static_status" id="#static_id"><a href="#static_url" title="#static_name">#static_name</a></li>', '', 2); ?>
									<!-- <li class="static_class cat_status"><a href="<?php// $plxShow->racine() ?>blog">Blog</a></li> -->

									<?php 
									// display a category at the end of the static pages list
									// https://pluxopolis.net/afficher-le-menu-d-une-categorie-dans-la-barre-des-menus-des-pages-statiques.html
									?>
									<?php
									if ($_SESSION['lang']=='fr'){
										$idCat = '004';
									}
									else{
										$idCat='002';
									}
								

									if(isset($plxMotor->aCats[$idCat])) {
										if($plxMotor->aCats[$idCat]['menu']=='oui' AND $plxMotor->aCats[$idCat]['active']) {
											$id = 'cat-'.intval($idCat);
											$url = $plxMotor->urlRewrite('?categorie'.intval($idCat).'/'.$plxMotor->aCats[$idCat]['url']);
											$racine = $plxShow->racine();
											$name = plxUtils::strCheck($plxMotor->aCats[$idCat]['name']);
											$active = ($plxShow->catId()==intval($idCat)?'active':'noactive');
											echo '<li id="'.$id.'" class="static menu '.$active.'"><a class="" title="'.$name.'" href="'.$racine.$url.'">'.$name.'</a></li>';
										}
									}
									?>

									<!-- <?php //$plxShow->catList('','<li class="static menu noactive"><a href="#cat_url" title="#cat_name">#cat_name</a></li>',4); ?> -->
									<!-- <?php// $plxShow->pageBlog('<li class="#page_class #page_status" id="#page_id"><a href="#page_url" title="#page_name">#page_name</a></li>'); ?> -->


									<?php  if ($_SESSION['lang']=='fr' ){  ?>

										<li class="#static_class #cat_status hotpandas"><a href="https://boutique.petitlutinartiste.fr/fr/">Boutique</a></li>

										<?php  }
										else if ($_SESSION['lang']=='en') { ?>

										<li class="menu hotpandas"><a href="https://boutique.petitlutinartiste.fr/en/">Shop</a></li>
									<?php } ?> 

										<div class="pub">
										<li><a href="https://framapiaf.org/@Petit_Lutin" title="Mastodon"><i class="fa fa-mastodon"></i></a></li>

											<li> <a href="https://www.facebook.com/PetitLutinArtiste/" title="Fesses-bouc"><i
														class="fa fa-facebook-f"></i></a></li>
											<li><a href="https://www.instagram.com/petit_lutin_artiste/" title="Insta(kilo)gram"><i
														class="fa fa-instagram"></i></a></li>
											<li><a href="https://www.youtube.com/@PetitLutinArtiste" title="Youtube"><i
														class="fa fa-youtube-play"></i></a></li>

											<li><a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="RSS"><i class="fa fa-rss"></i></a></li>
											<li>

												<?php  if ($_SESSION['lang']=='fr' ){  ?>
													<a href="<?php $plxShow->racine() ?>newsletter" title="Newsletter">
												<?php  }
												else if ($_SESSION['lang']=='en') { ?>
													<a href="<?php $plxShow->racine() ?>en/newsletter" title="Newsletter">
												<?php } ?> 
													<img src="<?php $plxShow->racine() ?>data/medias/newslutin_icon3.svg" width="22">
												</a>
											</li>

											<li><a href="https://liberapay.com/petitlutinartiste/"><i class="fa fa-liberapay" aria-hidden="true"></i></a></li>
											<li class="kofi"><a href="https://ko-fi.com/petitlutinartiste"><img  src="<?php $plxShow->racine() ?>/data/medias/kofi_blanc.png" width="30" ></a></li>
											<li class="tipeee"><a href="https://fr.tipeee.com/petit-lutin-artiste"><img  src="<?php $plxShow->racine() ?>/data/medias/tipeee_tip_btn.svg" width="25" ></a></li>
										</div>

								</ul>
							</div>
						</div>
					</nav>

				</div>

			</div>

		</div>


	</header>



<?php 
if ($_SESSION['lang']=='en') { ?>
<style>
	@media (min-width: 1100px) {
	.responsive-menu .menu {
		margin-top: -10px;
		}	
	}

</style>

<?php }

?>


	<style>

		@media (max-width:1099px){
			#langs {
				margin-left:auto;
			}
		}
		.heading-small {
			font-size: 3rem;
		}

		.h5 {
			font-size: 2rem;
		}


		header .menu li a {
			font-size: 2rem;
		}

		header .hotpandas a {
			color: rgb(196, 85, 159);

		}

		@media all and (min-width:900px) {
			.heading-small {
				font-size: 3rem;
				min-width: 300px;
			}

			.h5 {
				font-size: 2rem;
			}
		}

		.col.sml-4.navbar {
			/* width: 100%; */
			position: absolute;
			right: 1rem;
			z-index: 0;
		}

		.col.sml-2 {
			z-index: 500;
			display: flex;
			justify-content: center;
		}

		@media all and (min-width:601px) and (max-width:899px) {
			.heading-small {
				font-size: 2rem;
				min-width: 300px;
				/* width: 120px; */
			}

			.h5 {

				font-size: 1.1rem;
			}

			.nav {
				max-height: unset;
			}
		}

		@media all and (max-width:767px) {
			header .hotpandas a {
				background: #efefef;
			}
		}

		@media all and (max-width:600px) {
			.logo {
				display: flex;
				background-position-x: -1rem;
				padding-left: 7rem;
			}

			.heading-small {
				width: 300px;
				font-size: 2rem;
			}

			.logo h2 {
				display: none;
			}

			.ulLangs {
				position: relative;
				/* left: 7rem; */
			}

			/* label {
				width: 50px;
				position: absolute;
				right: 0;
			} */

			.nav {
				max-height: unset;
			}

			nav.menu {
				z-index: 600;
			}

			.bigZIndex li {
				z-index: 600;
			}

			ul.menu {
				/* padding-top: 10px; */
				text-align: center;
			}
		}

		@media all and (max-width:480px) {


			.heading-small {

				/* width: 120px; */
			}


		}
	</style>