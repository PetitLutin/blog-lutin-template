<?php include(dirname(__FILE__).'/header.php'); ?>

<main class="main">

	<div class="container">

		<div class="grid">


			<div class="content col sml-12 med-12">

				<?php //$plxShow->lastArtList('<a href="#art_url">#art_thumbnail</a>#cat_list<a href="#art_url" title="Lire la dernière news #art_title">#art_title</a>',6); ?>
				<h2>Derniers articles</h2>
				<div class="homeArticles">

					<?php //$plxShow->lastArtList('<li><a href="#art_url"> #art_thumbnail</a> <a href="#art_url" title="#art_title">#art_title</a> <a href="#art_url">#art_chapo</a></li>',5) ?>
					<?php $plxShow->lastArtList('<li> <a href="#art_url" title="#art_title">#art_chapo </a> </li>',3) ?>



				</div>

				<div class="more text-center"><a href="blog"><input type="submit" class="blue"
							value="Tous les articles"></a></div>

				<?php //$plxShow->artThumbnail(); ?>
				<?php //$plxShow->artChapo(); ?>





				<!-- <div class="bottomHome"> -->
				<!-- <nav class="pagination text-center">
						<?php $plxShow->pagination(); ?>
					</nav> -->

				<!-- <span style="text-align:center;">
						<?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
					</span> -->
			</div>
		</div>


	</div>

	</div>

</main>

<style>
	h2 {
		text-align: center;
	}

	.more {
		margin-top: 20px;
	}

	.homeArticles {
		display: flex;
		flex-wrap: wrap;

		justify-content: center;
		align-items: center;
		width: 100%;
	}

	.homeArticles li {
		list-style-type: none;
		/* padding: 15px; */
		margin: 15px;
		width: 400px;
		min-height: 420px;

		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		text-align: center;
	}

	.homeArticles li p {
		margin: 0;
		padding: 0;
		width: 400px;

	}

	.homeArticles li img {
		width: 400px;
		height: 400px;
		object-fit: cover;
		padding: 0;
		margin: 0;

	}

	@media all and (max-device-width: 400px) {
		.homeArticles li, .homeArticles li p {
			width: 300px;
		}
		.homeArticles li{
			height: 400px;
		}
		.homeArticles li img{
			width: 300px;

			height: 300px;
		}
	}
</style>


<?php include(dirname(__FILE__).'/footer.php'); ?>