<?php include __DIR__.'/header.php'; ?>

<main class="main">

	<div class="container">

		<div class="grid">

			<div class="content col sml-12">

			<div class="komika h1 text-center" style="width:100%">
			<?php if ($_SESSION['lang']=='fr' ){  ?>
			Le blog de Petit Lutin
			<?php } else { ?>
				Petit Lutin's blog
			<?php } ?>
		
			</div>

				<ul class="menu repertory">
					<div class="categoriesEtTags">
						<!-- catégories -->
						<ul class="categories">
							<?php $plxShow->catList('','<li id="#cat_id" class="#cat_status"><a href="#cat_url" title="#cat_name">#cat_name</a> (#art_nb)</li>'); ?>
						</ul>
						<!-- tags -->
						<ul class="tags">
							<?php $plxShow->tagList('<li><a class="#tag_status" href="#tag_url" title="#tag_name">#tag_name&nbsp;(#nb_art)</a></li>','', '') ?>
						</ul>
					</div>
				</ul>
				<!-- <div style="text-align:center"> <?php //eval($plxShow->callHook('MySearchForm')) ?></div> -->

				<div class="oldArticles">
					<?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

						

					<!-- <article class="article" id="post-<?php //echo $plxShow->artId(); ?>"> -->

					<!-- <header> -->
						<!-- <span class="art-date">
								<time datetime="<?php //$plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
									<?php //$plxShow->artDate('#num_day #month #num_year(4)'); ?>
								</time>
							</span>
							<h2>
								<?php //$plxShow->artTitle('link'); ?>
							</h2> -->



						<!-- <div>
							<small> -->
								<!-- <span class="written-by">
										<?php // $plxShow->lang('WRITTEN_BY'); ?> <?php //$plxShow->artAuthor() ?>
									</span> -->
								<!-- <span class="art-nb-com">
										<?php // $plxShow->artNbCom(); ?>
									</span> -->
							<!-- </small>
						</div>
						<div> -->
							<!-- <small>
								<span class="classified-in">
								<?php //$plxShow->lang('CLASSIFIED_IN') ?>
								: 
								<?php  //$plxShow->artCat() ?>
								</span>
								<span class="tags">
								<?php //$plxShow->lang('TAGS') ?>
								: 
								<?php  //$plxShow->artTags() ?>
								</span>
							</small> -->
						<!-- </div>
					</header> -->

					

					<?php //$plxShow->artThumbnail(); ?>
					<?php // permet d'afficher img complete et pas juste la miniature ?>
					
					<li>	
						<a href="<?php echo $plxShow->artUrl() ?>">
						<?php $plxShow->artThumbnail('<img class="" src="#img_url" alt="#img_alt" title="#img_title" />', true, false ) ?>
						</a>
					<?php $plxShow->artTitle('link'); ?>
					<small>
						<span class="art-date">
							<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
								<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
							</time>
						</span>
					</small>

					<div>
							<small><?php 
								$taglist = $plxShow->plxMotor->plxRecord_arts->f('tags');
								$artCatsStr = $plxShow->plxMotor->plxRecord_arts->f('categorie'); ?>
								
								<span class="classified-in">
								<?php //$plxShow->lang('CLASSIFIED_IN') ?>
							<?php if (!empty($artCatsStr)) {?>
								<?php  $plxShow->artCat(' ') ?> 
							<?php }?>
								</span>
								

								 <!-- <span class="tags"> -->
								 <?php if (!empty($taglist)) { ?>
									<?php $plxShow->artTags('<a href="#tag_url" title="#tag_name">#tag_name</a>',' ') ?>
								<?php } ?>
								<!-- </span>  -->
							</small>
						</div>
					</li>
					<!-- <ul> -->
						<!-- <a href="<?php //echo $plxShow->artUrl() ?>">
							<li><?php //$plxShow->artChapo(''); ?></li>
						</a> -->
					<!-- </ul> -->
					<!-- </article> -->

					<?php endwhile; ?>
				</div>
				<!-- <?php //$plxShow->lastArtList('<li> <a href="#art_url" title="#art_title">#art_chapo </a> </li>',3) ?> -->


				<nav class="pagination text-center">
					<?php $plxShow->pagination(); ?>
				</nav>

				<!-- <?php //$plxShow->artFeed('rss',$plxShow->catId(), '<span><a href="#feedUrl" title="#feedTitle">#feedName</a></span>'); ?> -->

			</div>

			<?php //include __DIR__.'/sidebar.php'; ?>

		</div>

	</div>

</main>

<?php include __DIR__.'/footer.php'; ?>
<style>
	.container {
		/* max-width: 1280px; */
		/* width: 100%; */

	}

	.grid {
		/*padding: 0;
						margin: auto;*/
		/* display: flex; */
		flex-wrap: wrap;
	}

	ul {
		padding: 0;
	}

</style>