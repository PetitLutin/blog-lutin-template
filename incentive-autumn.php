<?php
    if ($_SESSION['lang']=='fr' ){  ?>
        <div class="article-special-title">
            <h2 class="komika">C'est Halloween sur la boutique 🎃</h2>
            <div class="h3">Imprimez et créez vous-mêmes vos décorations !</div>
        </div>
        <div class="grid-products narrow-grid">
            <div class="grid-product">
                <div class="nice-border">
                    <a href="https://boutique.petitlutinartiste.fr/fr/agenda-et-bullet-journal/doodles-d-halloween-pour-bullet-journal-51">
                        <img src="data/medias/Boutique/printable-doodles-bullet-journal-halloween_2.jpg" alt="Des stickers d'Halloween à imprimer et à découper"></a>
                        <div class="figcaption">
                            <div class="komika">Stickers d'Halloween DIY pour bullet journal</div>
                            <div>
                                <a class="button" href="https://boutique.petitlutinartiste.fr/fr/agenda-et-bullet-journal/doodles-d-halloween-pour-bullet-journal-51">
                                    Télécharger</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid-product">
                    <div class="nice-border">
                        <a href="https://boutique.petitlutinartiste.fr/fr/halloween/citrouille-guirlande-d-halloween-a-fabriquer-52">
                            <img src="data/medias/Boutique/citrouille-guirlande-d-halloween-a-fabriquer-.jpg" alt="Guirlande d'Halloween DIY"></a>
                        <div class="figcaption">
                            <div class="komika">Guirlande citrouilles DIY</div>
                            <div><a class="button" href="https://boutique.petitlutinartiste.fr/fr/halloween/citrouille-guirlande-d-halloween-a-fabriquer-52">Télécharger</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
           

        <?php
        }
        else if ($_SESSION['lang']=='en')
            { ?>
                <div class="article-special-title">
                    <h2 class="komika">It's Halloween on the shop 🎃</h2>
                    <div class="h3">Print and create yourself your Halloween decors! </div>

                </div>
                <div class="grid-products narrow-grid">
                    <div class="grid-product">
                        <div class="nice-border">
                            <a href="https://boutique.petitlutinartiste.fr/en/agenda-and-bullet-journal/halloween-doodles-for-bullet-journal-51">
                                <img src="data/medias/Boutique/printable-doodles-bullet-journal-halloween_2.jpg" 
                                alt="Printable DIY stickers for Halloween">
                            </a>
                            <div class="figcaption">
                                <div class="komika">Halloween DIY stickers for bullet journal</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/agenda-and-bullet-journal/halloween-doodles-for-bullet-journal-51">
                                    Download</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid-product">
                        <div class="nice-border">
                            <a href="https://boutique.petitlutinartiste.fr/en/halloween/pumpkin-diy-halloween-decor-52">
                                <img src="data/medias/Boutique/citrouille-guirlande-d-halloween-a-fabriquer-.jpg" 
                                alt="Pumpkin printables for Halloween decor">
                            </a>
                            <div class="figcaption">
                                <div class="komika">DIY Halloween decor"</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/halloween/pumpkin-diy-halloween-decor-52">
                                    Download</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

    <?php }
