<?php
if ($_SESSION['lang']=='fr' ){  ?>
        <div class="article-special-title">
            <h2 class="komika">C'est Noël sur la boutique 🎄</h2>
            <div class="h3">Vos décorations de Noël DIY à imprimer et découper ❤️️</div>
        </div>
        <div class="grid-products narrow-grid">
            <div class="grid-product">
                <div class="nice-border">
                    <a href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/betises-de-chat-a-noel-fond-d-ecran-56">
                            <img src="data/medias/Illustrations/2023_12_24-symphonie-noel-chat/20231224_symphonie-noel_preview3.png"
                            alt="Fond d'écran illustré avec des chats qui font des bêtises dans un sapin de Noël">
                    </a>
                    <div class="figcaption">
                        <div class="komika">Fond d'écran "Les chats et le sapin de Noël"</div>
                        <div>
                            <a class="button" href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/betises-de-chat-a-noel-fond-d-ecran-56">
                            Télécharger</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid-product">
                <div class="nice-border">
                    <a href="https://boutique.petitlutinartiste.fr/fr/petit-lutin-de-noel/decoration-de-noel-a-fabriquer-pack-global-bonus-46">
                        <img src="data/medias/Illustrations/2023_12_09-planches-decoration-noel/gingerbread-candycane-santa-hat.jpg" 
                        alt="Décorations de Noël DIY en forme de bâton de canne à sucre, bonhomme en pain d'épices, et chapeau de Père Noël">
                    </a>
                    <div class="figcaption">
                        <div class="komika">Pack complet de décorations DIY</div>
                        <a class="button" href="https://boutique.petitlutinartiste.fr/fr/petit-lutin-de-noel/decoration-de-noel-a-fabriquer-pack-global-bonus-46">
                            Télécharger</a>
                    </div>
                </div>
            </div>
        </div>
           

        <?php
        }
        else if ($_SESSION['lang']=='en')
            { ?>
                <div class="article-special-title">
                    <h2 class="komika">Christmas products on the shop 🎄</h2>
                    <div class="h3">Your DIY Christmas decor that you can print and make at home ❤️️</div>

                </div>
                <div class="grid-products narrow-grid">
                    <div class="grid-product">
                        <div class="nice-border">
                            <a href="https://boutique.petitlutinartiste.fr/en/christmas/diy-christmas-decoration-star-42">
                                <img src="data/medias/Illustrations/2023_12_09-planches-decoration-noel/finished-star-on-tree.jpg" 
                                alt="A DIY Christmas decor in star shaped, on a tree">
                            </a>
                            <div class="figcaption">
                                <div class="komika">DIY Christmas decor, star pattern</div>
                                <div>
                                    <a class="button" href="https://boutique.petitlutinartiste.fr/en/christmas/diy-christmas-decoration-star-42">
                                    Download</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid-product">
                        <div class="nice-border">
                            <a href="https://boutique.petitlutinartiste.fr/en/christmas/diy-christmas-decor-global-bundle-bonus-46">
                                <img src="data/medias/Illustrations/2023_12_09-planches-decoration-noel/gingerbread-candycane-santa-hat.jpg" 
                                alt="DIY Christmas decor in shapes of candy cane, gingerbread man and Santa Claus hat, hung on a tree">
                            </a>
                            <div class="figcaption">
                                <div class="komika">DIY Christmas decor, global bundle with all patterns</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/christmas/diy-christmas-decor-global-bundle-bonus-46">
                                    Download</a></div>
                            </div>
                        </div>

                    </div>
                </div>

        <?php }