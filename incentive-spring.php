<?php
    if ($_SESSION['lang']=='fr' ){  ?>
        <div class="article-special-title">
            <h2 class="komika">C'est le printemps sur la boutique</h2>
            <div class="h3">Soutenez mon travail et ce blog en achetant un fond d'écran coloré pour votre ordinateur et votre smartphone 🌻🐟</div>
        </div>
        <div class="grid-products narrow-grid">
            <div class="grid-product">
                <div class="nice-border">
                    <a href="https://boutique.petitlutinartiste.fr/fr/tous-les-produits/poissons-d-avril-a-imprimer-57">
                        <img src="data/medias/Illustrations/2024_03_29-poisson-avril/petit-lutin-artiste-poisson-avril-phishing_2.jpg" 
                            alt="Autocollants de poissons d'avril à télécharger et à imprimer">
                    </a>
                    <div class="figcaption">
                        <div class="komika">Poissons d'avril à imprimer</div>
                        <div><a class="button" href="https://boutique.petitlutinartiste.fr/fr/tous-les-produits/poissons-d-avril-a-imprimer-57">Télécharger</a></div>
                    </div>
                </div>
            </div>
            <div class="grid-product">
                <div class="nice-border">
                <a href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/fond-d-ecran-du-printemps-chat-et-panda-36">
                    <img src="data/medias/Illustrations/2022_04_24-printemps/aperçu_fond-decran-printemps.png" 
                        alt="Fond d'écran où Moustache le panda et Symphonie le chat fluffy jouent dans un champ de fleurs jaunes"></a>
                    <div class="figcaption">
                        <div class="komika">Fond d'écran du printemps</div>
                        <div><a class="button" href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/fond-d-ecran-du-printemps-chat-et-panda-36">Télécharger</a></div>
                    </div>
                </div>
    <?php } 
    else if ($_SESSION['lang']=='en') { ?>
                <div class="article-special-title">
                    <h2 class="komika">It's spring on the shop</h2>
                    <div class="h3">Support my work and this blog by buying a colorful wallpaper for your computer and your smartphone 🌻🐟</div>

                </div>
                <div class="grid-products narrow-grid">
                    <div class="grid-product">
                        <div class="nice-border">
                        <a href="https://boutique.petitlutinartiste.fr/en/all-products/april-s-fool-pisces-to-print-57">
                            <img src="data/medias/Illustrations/2024_03_29-poisson-avril/petit-lutin-artiste-poisson-avril-phishing_2.jpg" 
                        alt="pisces stickers to download and print"></a>
                            <div class="figcaption">
                                <div class="komika">April's fools stickers</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/all-products/april-s-fool-pisces-to-print-57">Download</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-product">
                        <div class="nice-border">
                        <a href="https://boutique.petitlutinartiste.fr/en/wallpapers/spring-wallpaper-cat-and-panda-36">
                            <img src="data/medias/Illustrations/2022_04_24-printemps/aperçu_fond-decran-printemps.png" 
                                alt="Cute kawai wallpaper of a cat and a panda in a green field with a lot of yellow flowers"></a>
                            <div class="figcaption">
                                <div class="komika">Wallpaper "Spring"</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/wallpapers/spring-wallpaper-cat-and-panda-36">Download</a></div>
                            </div>
                        </div>

                    </div>
                </div>

                <?php }

