<?php
   if ($_SESSION['lang']=='fr' ){  ?>
    <div class="article-special-title">
        <h2 class="komika">C'est l´été sur la boutique</h2>
        <div class="h3">Soutenez mon travail et ce blog en achetant un fond d'écran coloré pour votre ordinateur et votre smartphone 💜</div>
    </div>
    <div class="grid-products narrow-grid">
        <div class="grid-product">
            <div class="nice-border">
                <a href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/le-bain-de-la-fee-fond-d-ecran-49"><img src="data/medias/Illustrations/2023_09_29-lexie-bubble-bath/petitlutinartiste-lexie-bubble-tea-bath-preview-carre.png" alt="Lexie la fée à la peau verte et aux ailes violettes se baigne dans un bain à bulles"></a>
                <div class="figcaption">
                    <div class="komika">Fond d'écran "Le bain de la fée"</div>
                    <div><a class="button" href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/le-bain-de-la-fee-fond-d-ecran-49">
                        Télécharger</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-product">
            <div class="nice-border">
            <a href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/dragon-bleu-fond-d-ecran-48"><img src="data/medias/Illustrations/2023_07_27-howtodragonyourtrain/howtodragonyourtrain_wallpaper_preview-square-.png" alt="Challenge How To Dragon Your Train, faire ressembler un train à un dragon. Un dragon bleu et rose comme un train Ouigo"></a>
                <div class="figcaption">
                    <div class="komika">Fond d'écran "How to dragon your train"</div>
                    <div><a class="button" href="https://boutique.petitlutinartiste.fr/fr/fonds-d-ecran/dragon-bleu-fond-d-ecran-48">
                        Télécharger
                    </a></div>
                </div>
            </div>

        </div>

       


    <?php
    }
    else if ($_SESSION['lang']=='en')
        { ?>
            <div class="article-special-title">
                <h2 class="komika">It's summer on the shop</h2>
                <div class="h3">Support my work and this blog by buying a colorful wallpaper for your computer and your smartphone 💜 </div>

            </div>
            <div class="grid-products narrow-grid">
                <div class="grid-product">
                    <div class="nice-border">
                        <a href="https://boutique.petitlutinartiste.fr/en/wallpapers/the-bath-of-the-fairy-wallpaper-49">
                            <img src="data/medias/Illustrations/2023_09_29-lexie-bubble-bath/petitlutinartiste-lexie-bubble-tea-bath-preview-carre.png" 
                            alt="Lexie the tiny fairy with green skin and purple wings is taking a bath in a bubble tea."></a>
                            <div class="figcaption">
                                <div class="komika">Wallpaper "The bath of the fairy"</div>
                                <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/wallpapers/the-bath-of-the-fairy-wallpaper-49">
                                    Download</a>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="grid-product">
                    <div class="nice-border">
                        <a href="https://boutique.petitlutinartiste.fr/en/wallpapers/blue-dragon-wallpaper-48">
                            <img src="data/medias/Illustrations/2023_07_27-howtodragonyourtrain/howtodragonyourtrain_wallpaper_preview-square-.png" 
                            alt="Challenge How To Dragon Your Train, which consisted in turning a train into a dragon. Here is a blue and pink dragon, 
                            like a french train Ouigo">
                        </a>
                        <div class="figcaption">
                            <div class="komika">Wallpaper "How to dragon your train"</div>
                            <div><a class="button" href="https://boutique.petitlutinartiste.fr/en/wallpapers/blue-dragon-wallpaper-48">Download</a></div>
                        </div>
                    </div>

                </div>
            </div>

    <?php }