<div class="text-center">
    <?php

$start_spring = new DateTime("2025-02-01", new DateTimeZone("Europe/Paris"));
$start_summer = new DateTime("2025-06-27", new DateTimeZone("Europe/Paris"));
$start_autumn = new DateTime("2025-10-13", new DateTimeZone("Europe/Paris"));
$start_winter = new DateTime("2025-11-30", new DateTimeZone("Europe/Paris"));

$today = new DateTime();


if($today > $start_winter) {
    include(dirname(__FILE__).'/incentive-christmas.php');
}  

elseif($today > $start_autumn) {
   include(dirname(__FILE__).'/incentive-autumn.php');
} 


elseif($today > $start_summer) {
    include(dirname(__FILE__).'/incentive-summer.php');
}

elseif($today > $start_spring) {
    include(dirname(__FILE__).'/incentive-spring.php');
}
         ?>
</div>

<style>
.article-special-title h2.komika{
    font-size:2.5rem;
}

.grid-product .komika {
	font-size:1.1em;
}

.figcaption{
    padding:5px;
    max-width:350px;
}

.grid-products{
    display: grid;
    grid-template-columns: repeat(2,1fr);
    gap: 10px;
    margin:25px 0 15px;
}
.grid-product{
    display: flex;
    flex-direction: column;
    align-items: center;
}
.grid-product img{
    /* width:100%;
    max-width:350px; */
    width:350px;
    max-height:350px;
    object-fit:cover;
}

.grid-product div{
    text-align: center;
}

@media all and (min-width:600px){
    .grid-products{
        display: grid;
        /* grid-template-columns: repeat(2,1fr); */
        gap: 20px;
    }
}
@media all and (min-width:1100px){
						
    .narrow-grid{
        max-width: 800px;
    }

    .grid-products{
        /* display: grid;
        grid-template-columns: repeat(2,1fr); */
        
        margin:25px auto 15px auto;
    }
    .grid-product{
        display: flex;
        flex-direction: column;
        align-items: center;
    }
}
</style>