<?php include(dirname(__FILE__).'/header.php'); ?>
<div class="bg"></div>


<main class="main">

	<div class="container">

		<div class="grid">

			<div class="content col sml-12 med-12">
				<!-- <div class="container"> -->

				<!-- <article class="article static" id="static-page-<?php //echo $plxShow->staticId(); ?>">

						<header>
							<h2>
								<?php //$plxShow->staticTitle(); ?>
							</h2>
						</header>

						<?php //$plxShow->staticContent(); ?>

					</article> -->

				<div class="komika h1 text-center">Bienvenue chez Petit Lutin !</div>

				<div class="h3 text-center" style="margin-top:15px">Je suis une <strong>illustratrice freelance</strong>, et je raconte mes anecdotes en BD sur ce blog. <br>
				Je parle aussi de <strong>musique</strong> (beaucoup), de <strong>logiciel libre</strong> (un peu), et de ce que c'est la vie avec un <strong>chat</strong> 🐱 (elle s'appelle Symphonie).</div>
				
				
				<!-- INCENTIVE -->
				<div class="text-center" style="margin-top:25px;">
					<?php include(dirname(__FILE__).'/incentive.php'); ?>
				</div>
				
				<!-- BLOG -->
				<div class="komika h1 text-center" style="width:100%;margin-top:45px">Les derniers articles sur le blog</div>

					<div class="oldArticles">
						<?php $plxShow->lastArtList('<li> <a href="#art_url" title="#art_title"><div>#art_thumbnail</div> <div>#art_title</div></a> <div><small>#num_day #month #num_year(4)</small></div> <div><small>#cat_list</small></div></li>',3) ?>
					</div>	
			
				<!-- COMMANDER ILLU -->
				<div class="text-center" style="width:100%;">
					<div class="komika h1" style="margin:auto; margin-top:45px;">Commander une illustration 🎨</div>
					<div class="grid-products narrow-grid grid-one">
						<div class="grid-product">
							<div style="display:flex;flex-direction:column;justify-content:center;height:100%">
								<div class="h2">Vous avez un projet ?</div>
								<div class="h3 pagination"><a class="p_next" href="mailto:bonjour@petitlutinartiste.fr">Parlons-en ! </a></div>
							</div>
						</div>
						<div class="grid-product">
							<div class="oldArticles">
								<li>
									<a href="https://petitlutinartiste.fr/les-coulisses-commande-illustration-avec-chat-et-black-metal">
									<div><img src="<?php $plxShow->racine() ?>/themes/blog/img/petitlutinartiste_coulisses-commande-client_petit.png" alt=""></div>
									<div>Les coulisses d'une commande d'illustration : avec un chat et du black metal ! 🐱🤘</div></a>
								</li>
							</div>
						</div>
					</div>
				</div>

				<!-- VIDEO -->
				<div class="komika h1 text-center" style="width:100%;margin-top:45px">Mes derniers projets en vidéo</div>

					<div class="oldArticles">
						
						<!-- <?php //$plxShow->lastArtList('<div class="miniature"> <a href="#art_url" title="#art_title">#art_chapo</a> </div>',3) ?> -->
						<?php $plxShow->lastArtList('<li> <a href="#art_url" title="#art_title"><div>#art_thumbnail</div> <div>#art_title</div></a> <div><small>#num_day #month #num_year(4)</small></div> <div><small>#cat_list</small></div></li>',3,7) ?>
					</div>	
					<!-- <p id="pagination"><?//php $plxShow->pagination(); ?></p>
						<?php //eval($plxShow->callHook('staticPagination')) ?> -->

			
					
				<style>
					.container {
						/* max-width: 1280px; */
						/* width: 100%; */

					}

					.grid {
						/*padding: 0;
						margin: auto;*/
						display: flex;
						flex-wrap: wrap;
					}

					/* .oldArticles {
						display: flex;
						flex-wrap: wrap;
						padding: 0;
						justify-content: center;
						margin: auto;
						width: 100%;
						margin-bottom:20px;
					} */

					/* .oldArticles .miniature {
						list-style-type: none;
						margin: 15px;
						min-width: 320px;
						height: 300px;
						box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
						text-align: center;
					} */

					/* .oldArticles .miniature p {
						margin: 0;
						padding: 0;
						width: 320px;
					}

					.oldArticles .miniature img {
						width: 320px;
						height: 200px;
						object-fit: cover;
						padding: 0;
						margin: 0;
					} */

					@media all and (min-device-width:1100px) {
						.container {
							padding-left: 3rem !important;
							padding-right: 3rem !important;
						}
					}

					.oldArticles {
						display: flex;
						flex-wrap: wrap;
						padding: 0;
						justify-content: center;
						margin: auto;
						width: 100%;
					}

					.oldArticles li {
						list-style-type: none;
						margin: 15px;
						/* min-width: 270px; */
						width: 350px;
						
						/* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); */

						/* box-shadow: 2px 2px 1px 2px #b5dab7; */
						box-shadow: 5px 5px 0 2px #b5dab7;
						border: 1px solid #383839;
						border-radius:10px;
						text-align: center;
						display: flex;
						flex-direction: column;
						justify-content: space-between;
					}

					.oldArticles li:hover{
						/* box-shadow: 5px 5px 0 2px #a47bcd; */
						box-shadow: 5px 5px 0 2px #a47bcd;

					}

					/* .oldArticles li p {
						margin: 0;
						padding: 0;
						width: 270px;
					} */

					.oldArticles li img {
						width: 350px;
						height: 200px;
						object-fit: cover;
						padding: 0;
						margin: 0;
						border-radius:10px 10px 0 0;

					}
					@media all and (max-width:599px){
						.grid-one{
							margin:25px 0 15px;
							grid-template-columns: 1fr;
						}

						.grid-one .grid-product:first-child{
							margin:25px 0 15px;
						}

						.narrow-grid .oldArticles li{
							width: unset;;
						}
					}

					@media all and (min-width:600px){
						.grid-products{
							display: grid;
							grid-template-columns: repeat(2,1fr);
							gap: 20px;
						}
					}
					@media all and (min-width:1100px){
						.oldArticles li{
							height: 300px;
						}
						
						.narrow-grid{
							max-width: 800px;
						}

						.grid-products{
							/* display: grid;
							grid-template-columns: repeat(2,1fr); */
							
							margin:25px auto 15px auto;
						}
						.grid-product{
							display: flex;
							flex-direction: column;
							align-items: center;
						}
					}
				</style>
			</div>

			<?php //include(dirname(__FILE__).'/sidebar.php'); ?>

		</div>

	</div>

</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>