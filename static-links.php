<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main">

		<div class="container">

			<div class="grid">

				<div class="content col sml-12 med-12">

					<article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

						<header>
							<h2>
								<?php $plxShow->staticTitle(); ?>
							</h2>
						</header>

						<div class="ig-links">
							<?php $plxShow->staticContent(); ?>
						</div>


						<style>
							article{
								text-align:center;
							}
						.ig-links p{
							padding:5px;
							border-radius:40px;	
						} 
						
						.ig-links p:first-child{
							color:rgb(196, 85, 159);
							border:1px solid rgb(196, 85, 159);
						}

						.ig-links p:nth-child(2){
							color:rgb(164, 123, 205);
							border:1px solid rgb(164, 123, 205);
						}

						.ig-links p:nth-child(3){
							color:rgb(36, 134, 191);
							border:1px solid rgb(36, 134, 191);
						}

						</style>

					</article>




				</div>

				<?php //include(dirname(__FILE__).'/sidebar.php'); ?>

			</div>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>

